#! /bin/bash

###################################
# Functions
###################################

function mysql_dump()
{

    local database_name=$1;
    local path_to=$2;

    cmd="mysqldump -h $MYSQL_HOST -P $MYSQL_PORT -u $MYSQL_USER";
    if [[ -n "$MYSQL_PASS" ]]; then cmd="$cmd -p$MYSQL_PASS"; fi;
    cmd="$cmd --complete-insert --dump-date $database_name";

    if [[ $preview_only == "t" ]]; then
        echo "$cmd > $path_to";
        return;
    fi;

    $cmd > $path_to;
} # mysql_dump

function pgsql_dump()
{
    local database_name=$1;
    local path_to=$2;

    cmd="pg_dump -h $PGSQL_HOST -p $PGSQL_PORT -U $PGSQL_USER --column-inserts --file=$path_to $database_name";

    if [[ $preview_only == "t" ]]; then
        echo "$cmd";
        return;
    fi;

    $cmd;

} # pgsql_dump

function upload_to_ftp()
{
    if [[ -n "$UPLOAD_ONE_FILE" ]]; 
        then
            cmd="lftp -u $FTP_USER,$FTP_PASS -c \"put $PATH_UPLOAD_ONE_FILE; exit;\"";
            if [[ $USE_SFTP == "t" ]]; 
                then cmd="$cmd sftp://$FTP_HOST"; 
                else cmd="$cmd $FTP_HOST";
            fi;
            if [[ $preview_only == "t" ]]; 
                then
                    echo "$cmd";
                    return;
                else
                    $cmd;
            fi;
        else
            for f in $PATH_BACKUP/*
            do
                cmd="lftp -u $FTP_USER,$FTP_PASS -c \"put $f; exit;\"";
                if [[ $USE_SFTP == "t" ]]; 
                    then cmd="$cmd sftp://$FTP_HOST"; 
                    else cmd="$cmd $FTP_HOST";
                fi;
                if [[ $preview_only == "t" ]]; then echo "$cmd"; continue; fi;
                $cmd;
            done
    fi;
} # upload_to_ftp

function upload_to_s3()
{
    s3cmd="$PATH_S3_BASH/s3-put -k $AWS_ACCESS_KEY -s $AWS_SECRET_KEY";

    if [[ -n "$UPLOAD_ONE_FILE" ]];
        then
            cmd1="tar --strip-components 1 -czf $PATH_UPLOAD_ONE_FILE $PATH_BACKUP";
            cmd2="$s3cmd -T $PATH_UPLOAD_ONE_FILE /$HOST_NAME/$UPLOAD_ONE_FILE";
            #cmd1="(cd `dirname $PATH_BACKUP` && tar -czf $UPLOAD_ONE_FILE `basename $PATH_BACKUP`)";
            #cmd2="(cd `dirname $PATH_BACKUP` && $s3cmd -T $UPLOAD_ONE_FILE /$HOST_NAME/$UPLOAD_ONE_FILE)";
            if [[ $preview_only == "t" ]]; 
                then
                    echo "$cmd1";
                    echo "$cmd2";
                    return;
                else
                    $cmd1;
                    $cmd2;
            fi;
        else
            for f in $PATH_BACKUP/*
            do
                cmd="$s3cmd -T $f /$HOST_NAME/$f";
                if [[ $preview_only == "t" ]]; then echo "$cmd"; continue; fi;
                $cmd;
            done
    fi;

} # upload_to_s3

###################################
# Configuration
###################################

# Script information.
AUTHOR="F.S. Davis <consulting@fsdavis.com>";
SCRIPT=`basename $0`;
DATE="2013-08-02";
VERSION="0.3.0-d";

# Exit codes.
EXIT_NORMAL=0;
EXIT_USAGE=1;
EXIT_INPUT=2;
EXIT_ENVIRONMENT=3;
EXIT_OTHER=9;

# Expected location of the S3 Bash library.
PATH_S3_BASH="/opt/s3-bash";

# MySQL defaults.
MYSQL_HOST="localhost";
MYSQL_PORT=3306;
MYSQL_USER="root";
MYSQL_PASS="";

# PostgreSQL defaults.
PGSQL_HOST="localhost";
PGSQL_PORT=5432;
PGSQL_USER="postgres";
PGSQL_PASS="";

# FTP defaults.
FTP_ENABLED="f";
FTP_HOST="ftp.example.com"
FTP_USER="user"
FTP_PASS="pass"
#FTP_PORT="21";
USE_SFTP="f";

# Other defaults.
CLEANUP_AFTER_UPLOAD="t";
ENABLE_S3="f";
EXCLUDE_FILE="";
HOST_NAME=`hostname`;
PATH_BACKUP="/var/backuper";
PATH_CONFIG="/etc/backuper/config.sh";
#UPLOAD_ONE_FILE="backups-`date +%u`.tgz";

# Temporary file.
#TEMP_FILE="/tmp/$SCRIPT".$$

###################################
# Help
###################################

HELP="
SUMMARY

Run a soft backup.

OPTIONS

-C <path>
    Path to backup config file. Default is $PATH_CONFIG.

-h		
    Print help and exit.

--help
    Print more help and exit.

-P
    Preview the steps, but don't actually do anything.

-v		
    Print version and exit.

--version
    Print full version and exit.

NOTES

Requirements

This script comes with a fraction of the options of backuper.py, but also has
no dependencies -- unless you want S3 upload (below). This script works
entirely within the limitations of Bash.

Configuration

Copy config.example.sh to /etc/backuper/ and customize to suite your needs.

Support for Amazon S3

If you want to upload to Amazon S3, you'll need to install a third-party tool:

    (cd /opt && git clone https://git@github.com/cosmin/s3-bash.git);

The bucket name defaults to $HOST_NAME, but can be overridden in the config.sh
file. Also, the bucket must already exist.

";

# Help and information.
if [[ $1 = '--help' ]]; then echo "$HELP"; exit $EXIT_NORMAL; fi;
if [[ $1 = '--version' ]]; then echo "$SCRIPT $VERSION ($DATE)"; exit $EXIT_NORMAL; fi;

###################################
# Arguments
###################################

path_config=$PATH_CONFIG;
preview_only="f";

while getopts "C:hPv" arg
do
    case $arg in
        C) path_config=$OPTARG;;
        P) preview_only="t";;
        v) echo "$VERSION"; exit;;
        h|*) echo "$SCRIPT <REQUIRED> [OPTIONS]"; exit;;
    esac
done

# Make sure the config file exists.
if [[ ! -f $path_config ]]; then
    echo "Config file does not exist: $path_config";
    exit $EXIT_INPUT;
fi;

###################################
# Procedure
###################################

# Source the config file.
source $path_config;

# UPLOAD_ONE_FILE is only a file name, so we need a path to it.
if [[ -n $UPLOAD_ONE_FILE ]]; then
    PATH_UPLOAD_ONE_FILE="/tmp/$UPLOAD_ONE_FILE";
fi;

# Fail early if S3 is requested but s3-bash is not installed.
if [[ $ENABLE_S3 == "t" ]]; then
    if [[ ! -d $PATH_S3_BASH ]]; then
        echo "Amazon S3 integration is enabled, but s3-bash is not installed.";
        exit $EXIT_ENVIRONMENT;
    fi;

    if [[ ! -f $AWS_SECRET_KEY ]]; then
        echo "Amazon S3 integration is enabled, but the path to the secret key does not exist: $AWS_SECRET_KEY";
        exit $EXIT_INPUT;
    fi;
fi;

# Make sure backup directory exists.
if [[ ! -d $PATH_BACKUP ]]; then
    echo "# Backup directory does not exist: $PATH_BACKUP (will be created)";
    cmd="mkdir -p $PATH_BACKUP";
    if [[ $preview_only == "t" ]]; 
        then echo "$cmd";
        else $cmd;
    fi;
    echo "";
fi;

# Make sure the exclude file exist if one is given.
if [[ -n "$EXCLUDE_FILE" ]]; then
    if [[ ! -f $EXCLUDE_FILE ]]; then
        echo "EXCLUDE_FILE does not exist: $EXCLUDE_FILE";
        exit $EXIT_INPUT;
    fi;
fi;

# Back up directories.
if [[ -n "$DIRECTORIES" ]]; then
    echo "# Backing up directories ...";
    for path in $DIRECTORIES
    do
        file_name="`basename $path`.tgz";
        cmd="tar -czf $PATH_BACKUP/$file_name $path";
        if [[ -n "$EXCLUDE_FILE" ]]; then cmd="$cmd --exclude $EXCLUDE_FILE"; fi;
        if [[ $preview_only == "t" ]]; then
            echo "$cmd";
            continue;
        fi;
        $cmd;
    done
    echo "";
fi;

# Back up mysql databases.
if [[ -n "$MYSQL_DATABASES" ]]; then
    echo "# Backing up MySQL databases ...";
    for db_name in $MYSQL_DATABASES
    do
        mysql_dump $db_name $BACKUP_PATH/$db_name.mysql;
    done
    echo "";
fi;

# Back up postgres databases.
if [[ -n "$PGSQL_DATABASES" ]]; then
    echo "# Backing up PostgreSQL databases ...";
    if [[ -n "$PGSQL_PASS" ]]; then
        export PGPASSWORD="$PGSQL_PASS";
    fi;

    for db_name in $PGSQL_DATABASES
    do
        pgsql_dump $db_name $BACKUP_PATH/$db_name.pgsql;
    done
    echo "";
fi;

# Upload via FTP.
if [[ $FTP_ENABLED == "t" ]]; then
    echo "# Attempting FTP upload to $FTP_HOST ...";
    upload_to_ftp;
    echo "";
fi;

# Push to S3.
if [[ $ENABLE_S3 == "t" ]]; then
    echo "# Attempting to upload to Amazon S3 ...";
    upload_to_s3;
    echo "";
fi;

# Clean up.
if [[ $CLEANUP_AFTER_UPLOAD == "t" ]]; then
    echo "# Cleaning up ...";
    cmd1="rm -Rf $PATH_BACKUP/*";
    cmd2="rm $PATH_UPLOAD_ONE_FILE";
    if [[ $preview_only == "t" ]]; 
        then
            echo "$cmd1";
            echo "$cmd2";
        else
            $cmd1;
            $cmd2;
    fi;
    echo "";
fi;

# Log the activity.
logger -i -t $SCRIPT "Soft backup executed using $path_config.";

# Clean up and exit.
#if [[ -f $TEMP_FILE ]]; then rm $TEMP_FILE; fi;
exit $EXIT_NORMAL;

# vim: set foldmethod=marker:
