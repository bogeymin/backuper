# Your Amazon keys for use with S3.
AWS_ACCESS_KEY = "asdf1234"
AWS_SECRET_KEY = "qwer5678"

# You may specify the path where tar files will be created or staged. An
# attempt will be made to recusively create this directory if it does not
# exist.
BACKUP_PATH = "/var/backuper"

# To keep things tidy, you may wish to clean up the soft backup files after
# they have been uploaded.  If you are copying files to an off-server
# directory or local directory, you'll want this setting to be False. Default
# is True.
CLEANUP_AFTER_UPLOAD = True

# Enter a list of directories you wish to backup.
DIRECTORIES = ("/etc", "/opt", "/var/lib", "here/", "/domains/example_com")

# Directories may also be specified with more detail. This is useful when you
# want to include additional context in the tar file name. For example:
"""
DIRECTORIES = {
    'etc-%(dayofweek)s.tgz': "/etc",
    'opt-%(dayofweek)s.tgz': "/opt",
    'var-lib-%(dayofweek)s.tgz': "/var/lib",
    'here.tgz': "here/",
    '%(basename)s.tgz': "/domains/example_com",
}
"""

# You may enable or disable upload to Amazon S3. When True, the AWS_ settings
# must be defined, and libcloud must be installed.
ENABLE_S3 = False

# You may specify an exclude file to be given to the tar command. At present,
# the exclude file is applied to all file or directory tarballs, so if you want
# per-directory exclusion, you'll need to create a separate config.py file and
# make another call to the backuper command.
EXCLUDE_FILE = "exclude.example.txt"

# The host name of the machine where this backup is running. If omitted, the
# name will be automatically determined. This is used as the directory or
# container name where the backup is sent.
HOST_NAME = "mac6"

# MySQL databases may be dumped and tarred on a per-host, per-database basis.
# If you need to backup more than one host, create a separate config file and
# make another call to the backuper command. The user must have enough access
# to databases on the host to perform a dump.
MYSQL_HOST = "mysql.example.com"
#MYSQL_PORT = 3306
MYSQL_USER = "mysql"
MYSQL_PASS = None
MYSQL_DUMP_SEPERATE = True

# List the MySQL databases to be dumped.
MYSQL_DATABASES = ("example1", "example2", "example3")

# MySQL databases may also be specified in dictionary form where the
# database name is the key and a context-sensitive file name is the value.
MYSQL_DATABASES = {
    'db1': "db1-mysql-%(dump)s-%(dayofweek)s.sql",
    'db2': "db2-mysql-%(dump)s-%(dayofweek)s.sql",
    'db3': "db3-mysql-%(dump)s-%(dayofweek)s.sql",
}

# NOT IMPLEMENTED: Failure notifications will send a message to the provided
# email address should the soft backup fail to run.
FAILURE_NOTIFICATIONS = (
    "dan@conpoint.com",
    "shawn@conpoint.com",
)

# PostgreSQL databases may be dumped and tarred on a per-host, per-database
# basis.  If you need to backup more than one host, create a separate config
# file and make another call to the backuper command. The user must have enough
# access to databases on the host to perform a dump.
PGSQL_HOST = "pgsql.example.com"
#PGSQL_PORT = 5432
PGSQL_USER = "postgres"
PGSQL_PASS = None
PGSQL_DUMP_SEPERATE = True

# List the PostgreSQL databases to be dumped.
PGSQL_DATABASES = ("example1", "example2", "example3")


# PostgreSQL databases may also be specified in dictionary form where the
# database name is the key and a context-sensitive file name is the value.
PGSQL_DATABASES = {
    'db1': "db1-pgsql-%(dump)s-%(dayofweek)s.sql",
    'db2': "db2-pgsql-%(dump)s-%(dayofweek)s.sql",
    'db3': "db3-pgsql-%(dump)s-%(dayofweek)s.sql",
}

# You may choose to upload a single tarball which will be created for all of
# the tarred files by naming the file to be uploaded. When omitted, the files
# are uploaded one at a time. Enabling this setting means a quicker upload, and
# disabling means more convenient downloads. Default is None.
UPLOAD_ONE_FILE = "backups-%(dayofweek)s.tgz"
