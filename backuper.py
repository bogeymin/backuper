#! /usr/bin/env python

"""
Backup files and databases.

OPTIONS

--config <file>
    Configuration file. Defaults to /etc/backuper/config.py -- see NOTES.

--debug
    Operate in debug mode; produce lots of output.

-h
    Print help and exit.

--help
    Print more help and exit.

--preview
    Preview only.

--quiet
    Produce no unnecessary output.

-v
    Print version and exit.

--version
    Print verbose version info and exit.

NOTES

Configuration File

The ``backuper`` command runs mostly from a configuration file. See
``config.example.py``.

Current Context

- ``basename`` The base name of the current file or directory being backed up.
- ``datestamp`` In the form of ``YYYMMDD``.
- ``dayofweek`` The current date of the week.
- ``dump`` Used with ``_DUMP_SEPERATE = True``, this will have a value of
  either ``data``, ``schema``, or ``both``.
- ``hostname`` The name of the host where the ``backuper`` command is running.
- ``unixtime`` A Unix timestamp.
- ``version`` The ``backuper`` version.

Uploads

Currently, ``backuper`` supports only one upload destination: Amazon S3.

Amazon S3

libcloud is used for Amazon S3 integration:

    pip install libcloud;

"""

# Python Imports
from commands import getstatusoutput
from datetime import datetime
import getopt
import imp
import os
import socket
import sys

# Third-Party Imports
try:
    from libcloud.storage.types import Provider, ContainerDoesNotExistError
    from libcloud.storage.providers import get_driver
    LIBCLOUD_ENABLED = True
except ImportError:
    LIBCLOUD_ENABLED = False

# Administrivia
__author__ = "F.S. Davis <consulting@fsdavis.com>"
__command__ = os.path.basename(sys.argv[0])
__date__ = "2013-07-24"
__version__ = "0.4.4-d"

# Constants
EXIT_OK = 0
EXIT_USAGE = 1
EXIT_INPUT = 2
EXIT_ENV = 3
EXIT_OTHER = 4
UPLOAD_FILES = list()

# Set up context variables.
CONTEXT = {
    'datestamp': datetime.now().strftime("%Y%m%d"),
    'dayofweek': datetime.now().strftime("%w"),
    'dump': "both",
    'hostname': socket.gethostname(),
    'unixtime': datetime.now().strftime("%s"),
    'version': __version__,
}

# Functions

def main():
    """Backup files and databases."""

    # Capture options and arguments.
    try:
        options, arguments = getopt.getopt(sys.argv[1:], "hv", ["config=", "debug", "help", "preview", "quiet", "version"])
    except getopt.GetoptError, err:
        print str(err)
        sys.exit(EXIT_USAGE)

    # Set defaults.
    be_quiet = False
    config_file = "/etc/backuper/config.py"
    debug_mode = False
    preview_only = False

    # Parse options.
    for name, value in options:
        if '-h' == name:
            print main.__doc__
            sys.exit(EXIT_OK)
        elif '--help' == name:
            print __doc__
            sys.exit(EXIT_OK)
        elif '-v' == name:
            print __version__
            sys.exit(EXIT_OK)
        elif '--version' == name:
            print "%s %s %s" %(__command__, __version__, __date__)
            sys.exit(EXIT_OK)
        elif '--config' == name:
            config_file = value
        elif '--debug' == name:
            debug_mode = True
        elif '--preview' == name:
            preview_only = True
        elif '--quiet' == name:
            be_quiet = True
        else:
            assert False, "Unhandled option: %s" %(name)

    # Source the config file.
    feedback("Loading the configuration file ...", be_quiet)
    config = load_config(config_file, enable_debug=debug_mode, enable_output=be_quiet, enable_preview=preview_only)

    # Make sure the given exclude file exists.
    if config.EXCLUDE_FILE and not os.path.exists(config.EXCLUDE_FILE):
        abort("Exclude file does not exist: %s" % config.EXCLUDE_FILE)

    # Create the /var/backuper directory as needed.
    if not os.path.exists(config.BACKUP_PATH) and not preview_only:
        try:
            os.mkdir(config.BACKUP_PATH)
        except OSError, e:
            abort("Could not create %s: %s" % (config.BACKUP_PATH, e))

    # Backup directories.
    feedback("Backing up directories ...", be_quiet)
    try:
        directories = config.DIRECTORIES
        debug("Directories give as %s." % type(directories), debug_mode, "main")
        debug("Total directories to backup: %s" % len(directories), debug_mode, "main")

        if type(directories) == tuple:
            for d in directories:
                backup_directory(config, d, debug_mode, preview_only)
        elif type(directories) == dict:
            for file_name, path in directories.items():
                # TODO: Use parse_path() for the directory backup path.
                CONTEXT['basename'] = os.path.basename(path)
                file_name = file_name % CONTEXT
                backup_directory(config, path, debug_mode, preview_only, file_name=file_name)
        else:
            abort("Unrecognized format for DIRECTORIES: %s" % type(directories), EXIT_INPUT)
    except AttributeError:
        debug("No DIRECTORIES configured.", debug_mode, "main")
        pass

    # Backup mysql databases.
    databases = config.MYSQL_DATABASES
    if len(databases) > 0:
        feedback("Backing up MySQL databases ...", be_quiet)
        backup_mysql_databases(config, databases, be_quiet, debug_mode, preview_only)

    # Backup postgresql databases.
    databases = config.PGSQL_DATABASES
    if len(databases) > 0:
        feedback("Backing up PostgreSQL databases ...", be_quiet)
        backup_pgsql_databases(config, databases, be_quiet, debug_mode, preview_only)

    # Tar up the entire backup directory if requested.
    if config.UPLOAD_ONE_FILE is not None:

        backup_file = config.UPLOAD_ONE_FILE % CONTEXT

        # Reset the files to be uploaded, replacing them with just one.
        config.UPLOAD_FILES = [backup_file,]

        # We need to clean up any temporary files now so they are not captured
        # in the one-file backup. But we also need to add the backup_file to be
        # cleaned up when we're done.
        feedback("Cleaning up temporary files before creating main backup ...")
        for f in config.CLEANUP_FILES:
            command = "rm %s" % f
            if preview_only:
                preview(command)
            else:
                getstatusoutput(command)
        config.CLEANUP_FILES = [backup_file,]

        # Tar up the entire backup directory.
        feedback("Creating main backup ...")
        command = "tar -czf %s %s" % (backup_file, config.BACKUP_PATH)
        if preview_only:
            preview(command)
        else:
            (status, output) = getstatusoutput(command)

    debug("Files to be uploaded: %s" % config.UPLOAD_FILES, debug_mode, "main")

    # Check AWS integration and upload if possible.
    if config.ENABLE_S3:
        if not LIBCLOUD_ENABLED:
            abort("Amazon S3 is enabled in the configuration, but libcloud is not installed.")
        else:
            for upload_file in config.UPLOAD_FILES:
                debug("Uploading %s to S3 %s ..." % (upload_file, config.HOST_NAME), debug_mode, "main")
                if not preview_only:
                    upload_to_s3(config.AWS_ACCESS_KEY, config.AWS_SECRET_KEY, config.HOST_NAME, upload_file, debug_mode)

    # Clean up.
    if config.CLEANUP_AFTER_UPLOAD:
        feedback("Cleaning up ...")
        for f in config.CLEANUP_FILES:
            command = "rm %s" % f
            if preview_only:
                preview(command)
            else:
                getstatusoutput(command)

    sys.exit()


def abort(message, exit_code=EXIT_OTHER):
    """Stop processing."""
    # TODO: Implement FAILURE_NOTIFICATIONS on abort().
    print "[abort] %s" % message
    sys.exit(exit_code)


def backup_directory(config, path, enable_debug=False, enable_preview=False, file_name=None):
    """Back up a directory.

    :param config: Settings returned by ``load_config()``.
    :type config: module

    :param path: The path to the directory to be backed up.
    :type path: str

    :param enable_debug: Whether to enable debugging output.
    :type enable_debug: bool

    :param enable_preview: Whether to execute command or just preview it.
    :type enable_preview: bool

    :param file_name: The name of the tar file. If omitted it will be defined automatically.
    :type file_name: str
    """

    def get_tar_path(path, file_name=None):
        """Get the full path to the tar file."""
        if file_name is not None:
            return "%s/%s" % (config.BACKUP_PATH, file_name)

        sanitized = path
        if sanitized[-1] == "/":
            sanitized = sanitized[:-1]
        if sanitized[0] == "/":
            sanitized = sanitized[1:]

        tar_file = sanitized.replace("/", "-")

        if tar_file[-1] == "/":
            tar_file = tar_file[:-1]

        tar_file += ".tgz"

        tar_path = "%s/%s" % (config.BACKUP_PATH, tar_file)

        return tar_path

    tar_path = get_tar_path(path, file_name=file_name)

    config.UPLOAD_FILES.append(tar_path)

    context = dict()
    context['tar_path'] = tar_path
    context['target'] = path

    command = "tar -czf %(tar_path)s"
    if config.EXCLUDE_FILE:
        command += " --exclude-from %(exclude_from)s"
        context['exclude_from'] = config.EXCLUDE_FILE
    command += " %(target)s"

    command = command % context

    if enable_preview:
        preview(command)
        return True

    (status, output) = getstatusoutput(command)
    if status == 0:
        return True
    return False


def backup_file(config, path, enable_debug=False, enable_preview=False, file_name=None):
    """Backup a file."""

    if file_name is not None:
        tar_path = file_name
    else:
        tar_path = "%s/%s.tgz" % (config.BACKUP_PATH, os.path.basename(path))

    command = "tar -czf %s" % tar_path
    if enable_preview:
        preview(command)
        return True

    (status, output) = getstatusoutput(command)
    if status == 0:
        return True
    return False


def backup_mysql_databases(config, databases, be_quiet=False, enable_debug=False, enable_preview=False):
    """Backup a MySQL databases identified in the config file."""

    # We'll store the database_name and path_to in a dictionary. The databases
    # variable will be converted based on its type.
    data_backups = dict()
    full_backups = dict()
    schema_backups = dict()
    if type(databases) == tuple:

        debug(config.MYSQL_DUMP_SEPERATE, enable_debug, "MYSQL_DUMP_SEPERATE")

        if config.MYSQL_DUMP_SEPERATE:
            abort("MYSQL_DUMP_SEPERATE is only supported when MYSQL_DATABASES has been given in dictionary form.", EXIT_INPUT)

        for database_name in databases:
            path_to = parse_path(config, "%s.mysql" % database_name)
            full_backups[database_name] = path_to
    elif type(databases) == dict:

        # Add databases to the backup.
        for database_name, path_to in databases.items():

            if config.MYSQL_DUMP_SEPERATE:
                data_dump = parse_path(config, path_to, extra_context={'dump': "data"})
                data_backups[database_name] = data_dump

                schema_dump = parse_path(config, path_to, extra_context={'dump': "schema"})
                schema_backups[database_name] = schema_dump
            else:
                path_to = parse_path(config, path_to, extra_context={'dump': "both"})
                full_backups[database_name] = path_to
    else:
        abort("Unrecognized format for MYSQL_DATABASES: %s" % type(databases), EXIT_INPUT)

    #debug(data_backups, enable_debug, "full_backups")

    # Iterating through the databases to be dump is now pretty easy.
    count = 0
    for database_name, path_to in data_backups.items():
        feedback("Dumping data for the %s database (MySQL) ..." % database_name)
        count += 1
        mysql_dump(config, database_name, path_to, enable_debug, enable_preview, include_schema=False)

        # When a single upload file is not used, we want to tar the dump file
        # and remove it.
        if not config.UPLOAD_ONE:
            backup_file(config, path_to, enable_debug, enable_preview)
            config.CLEANUP_FILES.append(path_to)
            config.UPLOAD_FILES.append(path_to + ".tgz")

    for database_name, path_to in full_backups.items():
        feedback("Dumping the %s database (MySQL) ..." % database_name)
        count += 1
        mysql_dump(config, database_name, path_to, enable_debug, enable_preview)

        # When a single upload file is not used, we want to tar the dump file
        # and remove it.
        if not config.UPLOAD_ONE:
            backup_file(config, path_to, enable_debug, enable_preview)
            config.CLEANUP_FILES.append(path_to)
            config.UPLOAD_FILES.append(path_to + ".tgz")

    for database_name, path_to in schema_backups.items():
        feedback("Dumping schema for the %s database (MySQL) ..." % database_name)
        count += 1
        mysql_dump(config, database_name, path_to, enable_debug, enable_preview, include_data=False)

        # When a single upload file is not used, we want to tar the dump file
        # and remove it.
        if not config.UPLOAD_ONE:
            backup_file(config, path_to, enable_debug, enable_preview)
            config.CLEANUP_FILES.append(path_to)
            config.UPLOAD_FILES.append(path_to + ".tgz")

    debug("Dumped %s databases ..." % count, enable_debug, "backup_mysql_databases")
    return count


def backup_pgsql_databases(config, databases, be_quiet, enable_debug=False, enable_preview=False):
    """Backup a PostgreSQL database."""

    # We'll store the database_name and path_to in a dictionary. The databases
    # variable will be converted based on its type.
    data_backups = dict()
    full_backups = dict()
    schema_backups = dict()
    if type(databases) == tuple:

        if config.PGSQL_DUMP_SEPERATE:
            abort("PGSQL_DUMP_SEPERATE is only supported when PGSQL_DATABASES has been given in dictionary form.", EXIT_INPUT)

        for database_name in databases:
            path_to = parse_path(config, "%s.pgsql" % database_name)
            full_backups[database_name] = path_to
    elif type(databases) == dict:

        # Add databases to the backup.
        for database_name, path_to in databases.items():

            if config.PGSQL_DUMP_SEPERATE:
                data_dump = parse_path(config, path_to, extra_context={'dump': "data"})
                data_backups[database_name] = data_dump

                schema_dump = parse_path(config, path_to, extra_context={'dump': "schema"})
                schema_backups[database_name] = schema_dump
            else:
                path_to = parse_path(config, path_to, extra_context={'dump': "both"})
                full_backups[database_name] = path_to
    else:
        abort("Unrecognized format for PGSQL_DATABASES: %s" % type(databases), EXIT_INPUT)


    # Iterating through the databases to be dump is now pretty easy.
    count = 0
    for database_name, path_to in data_backups.items():
        feedback("Dumping data for the %s database (PostgreSQL) ..." % database_name)
        count += 1
        pgsql_dump(config, database_name, path_to, enable_debug, enable_preview, include_schema=False)

        # When a single upload file is not used, we want to tar the dump file
        # and remove it.
        if not config.UPLOAD_ONE:
            backup_file(config, path_to, enable_debug, enable_preview)
            config.CLEANUP_FILES.append(path_to)
            config.UPLOAD_FILES.append(path_to + ".tgz")

    for database_name, path_to in full_backups.items():
        feedback("Dumping the %s database (PostgreSQL) ..." % database_name)
        count += 1
        pgsql_dump(config, database_name, path_to, enable_debug, enable_preview)

        # When a single upload file is not used, we want to tar the dump file
        # and remove it.
        if not config.UPLOAD_ONE:
            backup_file(config, path_to, enable_debug, enable_preview)
            config.CLEANUP_FILES.append(path_to)
            config.UPLOAD_FILES.append(path_to + ".tgz")

    for database_name, path_to in schema_backups.items():
        feedback("Dumping schema for the %s database (PostgreSQL) ..." % database_name)
        count += 1
        pgsql_dump(config, database_name, path_to, enable_debug, enable_preview, include_data=False)

        # When a single upload file is not used, we want to tar the dump file
        # and remove it.
        if not config.UPLOAD_ONE:
            backup_file(config, path_to, enable_debug, enable_preview)
            config.CLEANUP_FILES.append(path_to)
            config.UPLOAD_FILES.append(path_to + ".tgz")

    debug("Dumped %s databases ..." % count, enable_debug, "backup_pgsql_databases")
    return count


def debug(message, enable_debug=False, label=None):
    """Print a message if debug is enabled.

    :param message: The message to output.
    :type message: str

    :param enable_debug: Whether to enable output.
    :type enable_debug: bool

    :param label: A label to be pre-pended to the output.
    :type label: str
    """
    if enable_debug:
        if label is not None:
            message = "(%s) %s" % (label, message)
        print "[debug]", message


def feedback(message, be_quiet=False):
    """Print a message if enabled."""
    if not be_quiet:
        print message


def load_config(path="/etc/backuper/config.py", enable_debug=False, enable_output=True, enable_preview=False):
    """Load a configuration file."""

    # Config will be stored in a module object.
    module = imp.new_module("mysettings")

    # Set default configuration.
    module.__dict__['AWS_ACCESS_KEY'] = None
    module.__dict__['AWS_SECRET_KEY'] = None
    module.__dict__['BACKUP_PATH'] = "/var/backuper"
    module.__dict__['CLEANUP_AFTER_UPLOAD'] = True
    module.__dict__['CLEANUP_FILES'] = list()
    module.__dict__['DIRECTORIES'] = tuple()
    module.__dict__['ENABLE_S3'] = False
    module.__dict__['ENABLE_DEBUG'] = enable_debug
    module.__dict__['ENABLE_OUTPUT'] = enable_output
    module.__dict__['ENABLE_PREVIEW'] = enable_preview
    module.__dict__['EXCLUDE_FILE'] = None
    module.__dict__['FAILURE_NOTIFICATIONS'] = list()
    module.__dict__['HOST_NAME'] = socket.gethostname().replace("\.", "")
    module.__dict__['MYSQL_HOST'] = None
    module.__dict__['MYSQL_USER'] = None
    module.__dict__['MYSQL_PASS'] = None
    module.__dict__['MYSQL_PORT'] = None
    module.__dict__['MYSQL_DATABASES'] = tuple()
    module.__dict__['MYSQL_DUMP_SEPERATE'] = False
    module.__dict__['PGSQL_HOST'] = None
    module.__dict__['PGSQL_USER'] = None
    module.__dict__['PGSQL_PASS'] = None
    module.__dict__['PGSQL_PORT'] = None
    module.__dict__['PGSQL_DATABASES'] = tuple()
    module.__dict__['PGSQL_DUMP_SEPERATE'] = False
    module.__dict__['UPLOAD_FILES'] = list()
    module.__dict__['UPLOAD_ONE_FILE'] = None
    module.__dict__['UPLOAD_ONE'] = False

    # Load the file.
    try:
        execfile(path, module.__dict__)
    except IOError, e:
        print 'Unable to load configuration file %s (%s)' % (path, e.strerror)
        sys.exit(EXIT_INPUT)
    sys.modules['mysettings'] = module

    # This makes working with the UPLOAD_ONE_FILE a little cleaner.
    if module.UPLOAD_ONE_FILE is not None:
        module.UPLOAD_ONE = True

    return module


def mysql_dump(config, database_name, path_to, enable_debug=False, enable_preview=False, include_data=True, include_schema=True):
    """Dump a MySQL database."""

    # The command will be assembled from tokens.
    tokens = list()
    tokens.append("mysqldump")

    if config.MYSQL_HOST:
        tokens.append("-h %s" % config.MYSQL_HOST)
    if config.MYSQL_PORT:
        tokens.append("-P %s" % config.MYSQL_PORT)
    if config.MYSQL_USER:
        tokens.append("-u %s" % config.MYSQL_USER)
    if config.MYSQL_PASS:
        tokens.append("-p%s" % config.MYSQL_PASS)

    if include_data and not include_schema:
        tokens.append("--no-create-info")
    elif include_schema and not include_data:
        tokens.append("--no-data")
    else:
        pass

    if include_data:
        tokens.append("--complete-insert")

    tokens.append("--dump-date")
    tokens.append("%s > %s" % (database_name, path_to))
    command = " ".join(tokens)

    if enable_preview:
        preview(command)
        return True

    (status, output) = getstatusoutput(command)
    if status == 0:
        return True
    return False


def parse_path(config, path, extra_context=None):
    """Return the path to a backup file."""

    # We start with the global context.
    context = CONTEXT

    # Add extra context if it's given.
    if type(extra_context) == dict:
        for key, value in extra_context.items():
            context[key] = value

    # Parse the context into the path string.
    actual_path = path % context

    # Add the BACKUP_PATH.
    actual_path = "%s/%s" % (config.BACKUP_PATH, actual_path)

    return actual_path


def pgsql_dump(config, database_name, path_to, enable_debug=False, enable_preview=False, include_data=True, include_schema=True):
    """Dump a PostgreSQL database."""

    # The command will be assembled from tokens.
    tokens = list()

    # HACK: Using export for PGPASSWORD is messy. There are various ways to
    # deal with the password, and this was the quickest to use in this context.
    # http://stackoverflow.com/a/15593100/241720
    if config.PGSQL_PASS:
        tokens.append('export PGPASSWORD="%s";' % config.PGSQL_PASS)

    tokens.append("pg_dump")

    if config.PGSQL_HOST:
        tokens.append("-h %s" % config.PGSQL_HOST)
    if config.PGSQL_PORT:
        tokens.append("-p %s" % config.PGSQL_PORT)
    if config.PGSQL_USER:
        tokens.append("-U %s" % config.PGSQL_USER)

    if include_data and not include_schema:
        tokens.append("--data-only")
    elif include_schema and not include_data:
        tokens.append("--schema-only")
    else:
        pass

    if include_data:
        tokens.append("--column-inserts")

    tokens.append("--file=%s" % (path_to))
    tokens.append(database_name)
    command = " ".join(tokens)

    if enable_preview:
        preview(command)
        return True

    (status, output) = getstatusoutput(command)
    if status == 0:
        return True
    return False


def preview(command):
    """Print a command."""
    print "[preview]", command


def upload_to_s3(key, secret, container_name, path, enable_debug=False):
    """Upload files to Amazon S3.

    :param key: The AWS access key.
    :type key: string

    :param secret: The AWS secret key.
    :type secret: string

    :param container_name: The S3 container to use. It will be created if it
                           does not already exist.
    :type container_name: string

    :param path: The path to the file to be uploaded.
    :type path: string

    :param enable_debug: Indicates whether to print debug messages.
    :type enable_debug: bool
    """

    # Get the driver.
    driver = get_driver(Provider.S3)(key, secret)

    # Create a container if it doesn't already exist
    try:
        debug("Loading the S3 container ...", enable_debug, "upload_to_s3")
        container = driver.get_container(container_name=container_name)
    except ContainerDoesNotExistError:
        debug("Attempting to create the container ...", enable_debug, "upload_to_s3")
        container = driver.create_container(container_name=container_name)

    # Upload the file.
    debug("Uploading object ...", enable_debug, "upload_to_s3")
    Obj = driver.upload_object(path, container, os.path.basename(path))
    debug('Upload complete, transferred: %s KB' % ((Obj.size / 1024)), enable_debug, "upload_to_s3")


# Kickoff
if __name__ == "__main__":
	main()

