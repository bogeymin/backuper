# Backuper

A stupid, simple way to create soft backups of files and databases.

## System Requirements

1. Python 2.6 or later is required to run the Python script.
2. The Bash script has been tested with Bash 4.x but should work with most
   versions of bash.

## Difference Between Python and Bash Versions

The Python script provides a number of features not available to Bash, and
these differences are best observed in the example configuration files. The
Bash script requires only bash and a small, third-party library if Amazon S3
integration is desired. It is far less capable than the Python script, but
will run on more systems.

## Installation

### backuper

The backuper directory can live anywhere. `/opt/backuper` is suggested:

    cd /opt;
    git clone https://git@bitbucket.org/bogeymin/backuper.git;

> Note: If you are running on a system where git is not installed, you can use
also download by version. For example:

    cd /opt;
    wget --no-check-certificate https://bitbucket.org/bogeymin/backuper/get/v0.4.3-d.tar.gz;
    tar -xzf v0.4.3-d.tar.gz;
    mv bogeymin-backuper-* backuper;
    rm -f v0.4.3-d.tar.gz;

### libcloud

The Python script utilizes libcloud for Amazon S3 uploads:

    pip install apache-libcloud;

### s3-bash

The Bash script utilizes s3-bash for Amazon S3 uploads:

    (cd /opt && git clone https://git@github.com/cosmin/s3-bash.git);

> Note: As with the backuper repo, you can download the package if git is not
installed. For example:

    cd /opt;
    wget --no-check-certificate https://github.com/cosmin/s3-bash/archive/master.zip;
    unzip master;
    mv s3-bash-master s3-bash;
    rm -f master;

### Configuration

Create the configuration directory and copy the desired config file(s):

    cd /opt/backuper;
    test -d /etc/backuper || mkdir /etc/backuper;
    cp config.example.py /etc/backuper/config.py;
    cp config.example.sh /etc/backuper/config.sh;

> Note: It is possible to use more than one configuration file.

### Cron Job

Set up a cron job to run the backup. For example, this would run at the end of
each day:

    59 23 * * * /opt/backuper/backuper.py > /dev/null 2>&1

## Official Version

The official version of the package follows the version of the Python script.
However, the Bash script has it's own version as well. Use `--version` with
either script to check the version.

## Some Ideas

### Selective Backups

Make it possible to selectively execute certain backups:

    --databases
        Backup only the `_DATABASES`. This is useful for selectively re-doing a
        backup without executing the other instructions from the `config.py` file.

    --directories
        Backup only the `DIRECTORIES`. This is useful for selectively re-doing a
        backup without executing the other instructions from the `config.py` file.

### Individual Files

Official support for backing up individual files, i.e. a `FILES` variable.

> Note: This already exists with the Python script, but is not exposed for use
from the config file.
