# Your Amazon keys for use with S3. Note that the secret key file *cannot*
# contain a newline character. See
# http://www.performantdesign.com/2009/09/14/s3-bash-no-newline-the-secret-key-file/
AWS_ACCESS_KEY="asdf1234";
AWS_SECRET_KEY="secret.example.key";

# You may specify the path where tar files will be created or staged. An
# attempt will be made to recusively create this directory if it does not
# exist.
BACKUP_PATH="/var/backuper";

# To keep things tidy, you may wish to clean up the soft backup files after
# they have been uploaded.  If you are copying files to an off-server
# directory, you'll want this setting to be False. Default is "t".
CLEANUP_AFTER_UPLOAD="t";

# Enter a list of directories you wish to backup. You MUST specify the full
# path. Symlinks aren't allowed.
DIRECTORIES="/etc
/opt
/var/domains";

# You may enable or disable upload to an FTP server and also set the FTP
# options here.
FTP_ENABLED="f";
FTP_HOST="ftp.example.com"
FTP_USER="user"
FTP_PASS="pass"
#FTP_PORT="21";
FTP_SECURE="f";

# You may enable or disable upload to Amazon S3. When True, the AWS_ settings
# must be defined, and s3-bash must be installed. Defaults to "f".
ENABLE_S3="f";

# You may specify an exclude file to be given to the tar command. At present,
# the exclude file is applied to all file or directory tarballs, so if you want
# per-directory exclusion, you'll need to create a separate config.py file and
# make another call to the backuper command.
EXCLUDE_FILE="exclude.example.txt";

# The host name of the machine where this backup is running. If omitted, the
# name will be automatically determined. This is used as the directory or
# container name where the backup is sent.
HOST_NAME="mac6";

# MySQL databases may be dumped and tarred on a per-host, per-database basis.
# If you need to backup more than one host, create a separate config file and
# make another call to the backuper command. The user must have enough access
# to databases on the host to perform a dump.
MYSQL_HOST="localhost";
#MYSQL_PORT=3306;
MYSQL_USER="root";
MYSQL_PASS="";

# List the MySQL databases to be dumped.
MYSQL_DATABASES="example1
example2
example3";

# PostgreSQL databases may be dumped and tarred on a per-host, per-database
# basis.  If you need to backup more than one host, create a separate config
# file and make another call to the backuper command. The user must have enough
# access to databases on the host to perform a dump.
PGSQL_HOST="localhost";
#PGSQL_PORT=5432;
PGSQL_USER="postgres";
PGSQL_PASS="";

# List the PostgreSQL databases to be dumped.
PGSQL_DATABASES="example1
example2
example3";

# You may choose to upload a single tarball which will be created for all of
# the tarred files by naming the file to be uploaded. When omitted, the files
# are uploaded one at a time. Enabling this setting means a quicker upload, and
# disabling means more convenient downloads. Default is None.
UPLOAD_ONE_FILE="backups-`date +%u`.tgz"
